from django.urls import path

from Arena.views import client_list, client_new, client_update, client_delete, index

from Arena.views import  equipe_new, equipe_update, equipe_delete

from Arena.views import pedido_list, pedido_new, pedido_update, pedido_delete

urlpatterns = [
    
    path('client_list', client_list, name="client_list"),
    path('new/', client_new, name="client_new"),
    path('update/<int:id>', client_update, name="client_update"),
    path('delete/<int:id>', client_delete, name="client_delete"),
    
    path('', index, name="index"),
    # path('equipe_list', equipe_list, name="equipe_list"),
    path('new_equipe/', equipe_new, name="equipe_new"),
    path('update_equipe/', equipe_update, name="equipe_update"),
    path('delete_equipe/<int:id>', equipe_delete, name="equipe_delete"),

    path('pedido_list', pedido_list, name="pedido_list"),
    path('new_pedido/', pedido_new, name="pedido_new"),
    path('update_pedido/', pedido_update, name="pedido_update"),
    path('delete_pedido/<int:id>', pedido_delete, name="pedido_delete"),



]