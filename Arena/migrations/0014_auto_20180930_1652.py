# Generated by Django 2.0.6 on 2018-09-30 19:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Arena', '0013_teste'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Teste',
        ),
        migrations.RemoveField(
            model_name='equipe',
            name='integrante',
        ),
        migrations.AddField(
            model_name='equipe',
            name='integrantes',
            field=models.ManyToManyField(null=True, to='Arena.Cliente', verbose_name='Integrante'),
        ),
    ]
