# Generated by Django 2.1.1 on 2018-09-29 22:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Arena', '0011_remove_equipe_integrante'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipe',
            name='integrante',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='Arena.Cliente', verbose_name='Integrante'),
        ),
    ]
