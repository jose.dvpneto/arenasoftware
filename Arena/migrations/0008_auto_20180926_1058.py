# Generated by Django 2.1.1 on 2018-09-26 13:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Arena', '0007_auto_20180926_1057'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipe',
            name='descricao',
            field=models.TextField(help_text='A valid email address, please.', max_length=999),
        ),
    ]
