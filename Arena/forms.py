from django.forms import ModelForm
from Arena.models import Cliente
from Arena.models import Equipe
from Arena.models import Pedido

class ClienteForm(ModelForm):
    class Meta:
        model = Cliente
        fields = ['first_name', 'last_name', 'idade', 'photo','pedido']

class EquipeForm(ModelForm):
    class Meta:
        model = Equipe
        fields = ['nome_grupo', 'insignia', 'descricao']

class PedidoForm(ModelForm):
    class Meta:
        model = Pedido
        fields = ['data_pedido', 'tipo_pedido']
