def :
    rom django.shortcuts import render, redirect, get_object_or_404

from Arena.models import Cliente
from Arena.models import Equipe
from Arena.models import Pedido
from Arena.models import Evento

from .forms import ClienteForm
from .forms import EquipeForm
from .forms import PedidoForm
#from .forms import PedidoCliente


# Create your views here.
#create
def client_new(request):
    form = ClienteForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        #retornar para uma pagina
        return redirect('client_list')
    return render(request, 'form.html', {'form':form})

#read
def client_list(request, template_name='index.html'):
    cliente = Cliente.objects.all()
    dados = {'verClientes': cliente}
    return render(request, 'index.html', dados)

#update
def client_update(request, id):
    cliente = get_object_or_404(Cliente, pk=id)
    form = ClienteForm(request.POST or None, request.FILES or None, instance=cliente)

    if form.is_valid():
        form.save()
        return redirect('client_list')
    return render(request, 'form.html', {'form':form})

#delete
def client_delete(request, id):
    cliente = get_object_or_404(Cliente, pk=id)

    if request.method == 'POST':
        
        cliente.delete()
        return redirect('client_list')

    return render(request, 'cliente_delete_confirm.html', {'cliente':cliente})



"""
aqui começa o CRUD das Equipes
"""

#read
def index(request):
    equipe = Equipe.objects.all()[0:4]
    evento = Evento.objects.all()
     
    dados = {'listEquipes': equipe, 'eventos': evento}
    return render(request, 'index.html', dados)

    # equipe = {}
    # equipe['nome_grupo'] = Equipe.objects.all()
    # return render(request, 'equipe_list.html', equipe)

#create new group
def equipe_new(request):
    form = EquipeForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        #retornar para uma pagina
        return redirect('client_list')
    return render(request, 'form.html', {'form':form})

#update
def equipe_update(request, id):
    equipe = get_object_or_404(Equipe, pk=id)
    form = EquipeForm(request.POST or None, request.FILES or None, instance=equipe)

    if form.is_valid():
        form.save()
        return redirect('client_list')
    return render(request, 'form.html', {'form':form})

#delete
def equipe_delete(request, id):
    equipe = get_object_or_404(Cliente, pk=id)

    if request.method == 'POST':
        
        equipe.delete()
        return redirect('client_list')

    return render(request, 'equipe_delete_confirm.html', {'equipe':equipe})

"""
aqui começa o CRUD dos Pedidos
"""

#create
def pedido_new(request):
    form = PedidoForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        #retornar para uma pagina
        return redirect('client_list')
    return render(request, 'form.html', {'form':form})

#read
def pedido_list(request, template_name='index.html'):
    pedido = Pedido.objects.all()
    dados = {'verPedidos': pedido}
    return render(request, 'index.html', dados)

#update
def pedido_update(request, id):
    pedido = get_object_or_404(Pedido, pk=id)
    form = PedidoForm(request.POST or None, request.FILES or None, instance=pedido)

    if form.is_valid():
        form.save()
        return redirect('client_list')
    return render(request, 'form.html', {'form':form})

#delete
def pedido_delete(request, id):
    pedido = get_object_or_404(Pedido, pk=id)

    if request.method == 'POST':
        
        pedido.delete()
        return redirect('client_list')

    return render(request, 'pedido_delete_confirm.html', {'pedido':pedido})


"""
aqui começa o CRUD das 
"""