from django.contrib import admin
from . import models
from .models import  Cliente
from .models import  Equipe
from .models import  Endereco
from .models import  Pedido
from .models import  Evento

# from .models import  Teste



# Register your models here.

admin.site.register(models.Cliente)
admin.site.register(models.Equipe)
admin.site.register(models.Endereco)
admin.site.register(models.Pedido)
admin.site.register(models.Evento)

# admin.site.register(models.Teste)