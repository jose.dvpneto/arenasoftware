from django.db import models
from datetime import datetime


# Create your models here.

class Pedido(models.Model):
    data_pedido = models.DateField()
    tipo_pedido = models.CharField(max_length=30)

    def __str__(self):
        return self.tipo_pedido

class Cliente(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    idade = models.DateField(null=True, blank=True)
    photo = models.ImageField(upload_to='clientes_photos', null=True, blank=True)
    pedido = models.ForeignKey(Pedido, verbose_name="Pedido", on_delete=models.SET_NULL, null=True)


    #define o que vai ser mostrado no painel Admin

    def __str__(self):
        return self.first_name + ' ' + self.first_name


class Equipe(models.Model):
    nome_grupo = models.CharField(max_length=30)
    insignia = models.ImageField(upload_to='insignia_clientes', null=True, blank=True)
    descricao = models.TextField(max_length=999, help_text='Descrição do grupo.')
    pontos = models.CharField(max_length=30, help_text='Pontos da equipe', null=True)
    integrantes = models.ManyToManyField(Cliente, verbose_name="Integrante", null=True)

    def __str__(self):
        return self.nome_grupo

class Endereco(models.Model):
    estado = models.CharField(max_length=2)
    cidade = models.CharField(max_length=15)
    logradouro = models.CharField(max_length=200)
    numero = models.IntegerField()
    complemento = models.CharField(max_length=20)
    bairro = models.CharField(max_length=20)
    cep = models.IntegerField()

    def __str__(self):
        return self.cidade

class Evento(models.Model):
    nome_evento = models.CharField(max_length=30)
    descricao = models.TextField(max_length=999, help_text='Descrição do grupo.')
    custo_pontos = models.CharField(max_length=30, help_text='Pontos da equipe', null=True)
    data_inicio = models.DateTimeField(default=datetime.now(), blank=True)
    data_fim = models.DateTimeField(default=datetime.now(), blank=True)
    participantes = models.ManyToManyField(Cliente, verbose_name="Participantes", null=True)

    def __str__(self):
        return self.nome_evento

