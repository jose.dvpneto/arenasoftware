from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def lerDoBanco(nome):
    lista_nomes = [
        {'nome':'Ana', 'Idade':'20'},
        {'nome':'Paula', 'Idade':'25'}
    ]
    for pessoa in lista_nomes:
        if pessoa ['nome'] == nome:
            return pessoa
        else:
            return {'nome': 'Não encontrado','idade': 0 }

def fname(request, nome):
    result =  lerDoBanco(nome)
    if result ['idade'] > 0:
        return HttpResponse('A pessoa foi encontrada, ela tem' + str(result['idade']) + ' anos')
    else:
        return HttpResponse('Pessoa não encontrada')